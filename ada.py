
import discord
from discord.ext import commands
import asyncio
import os
import certifi
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
http_seguro = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
bot = commands.Bot(command_prefix='&', description='Al servicio de vuestra merced.', pm_help=True)
extensiones = ['miscelaneo', 'shitpost_safebooru', 'shitpost_cloudinary']

@bot.event
async def on_ready():
    print('¡Me conecté a Discord! ^_^')
    print((('Mi usuario con discriminador es: ' + bot.user.name) + '#') + bot.user.discriminator)
    print('Mi ID en Discord es: ' + str(bot.user.id))
    print('~~~~~~')
if __name__ == '__main__':
    for extension in extensiones:
        try:
            bot.load_extension('cogs.' + extension)
        except Exception as e:
            exc = f'''{type(e).__name__}: {e}'''
            print(f'''Percibí un fallo al cargar la extensión {extension}:\n{exc}''')
    bot.run(os.environ['DISCORD_BOT_TOKEN'])
