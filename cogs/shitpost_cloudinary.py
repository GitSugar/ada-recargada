
import discord
from discord.ext import commands
import asyncio
import random
import os
import cloudinary
import cloudinary.api
cloudinary.config(cloud_name=os.environ['CLOUDINARY_CLOUD_NAME'], api_key=os.environ['CLOUDINARY_API_KEY'], api_secret=os.environ['CLOUDINARY_API_SECRET'])

class ShitpostCloudinary():

    def __init__(self, bot):
        self.bot = bot

    def cloudinary_shitpost_get(self, carpeta):
        'Consigo imágenes de mi Cloudinary en base a una ruta.'
        lista_de_imagenes = cloudinary.api.resources(type='upload', max_results=500, prefix=carpeta + '/')['resources']
        random.shuffle(lista_de_imagenes)
        return lista_de_imagenes[0]['secure_url']

    @commands.command()
    async def siro(self, ctx):
        'Paso una imagen de Siro.'
        embed = discord.Embed()
        embed.set_image(url=self.cloudinary_shitpost_get('siro'))
        await ctx.send(embed=embed)

    @commands.command()
    async def laura(self, ctx):
        'Paso una imagen de Laura Dreyfuss (para Rum).'
        embed = discord.Embed()
        embed.set_image(url=self.cloudinary_shitpost_get('rum/laura'))
        await ctx.send(embed=embed)

    @commands.command()
    async def cafe(self, ctx):
        'Le sirvo un café. Puede aclarar "negro" o "cortado".'
        subcarpetas = ['negro', 'cortado']
        carpeta = 'cafe'
        descripcion = "A la orden, un café"
        if ctx.message.content[6:] in subcarpetas:
            carpeta += '/' + ctx.message.content[6:]
            descripcion += ' ' + ctx.message.content[6:]
        embed = discord.Embed(description=descripcion + ". :coffee:")
        embed.set_image(url=self.cloudinary_shitpost_get(carpeta))
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(ShitpostCloudinary(bot))
