
import discord
from discord.ext import commands
import asyncio
import random

class Miscelaneo():

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def cambiarestado(self, ctx):
        'Cambio el "juego" al que estoy jugando en Discord.'
        lista_de_estados = ['c:', ':o', 'uwu', 'owo', ':x', 'testeadísimo', 'maincra']
        random.shuffle(lista_de_estados)
        await self.bot.change_presence(activity=discord.Game(name=lista_de_estados[0]))

    @commands.command()
    async def hola(self, ctx):
        'Respondo con "ola".'
        await ctx.send('ola')

    @commands.command()
    async def anismanlomataron(self, ctx):
        'Respondo si a Nisman lo mataron.'
        await ctx.send('Sí.')

    @commands.command()
    async def sentience(self, ctx):
        'Respondo si tengo sintiencia.'
        await ctx.send('Por ahora, no. :thinking:')

    @commands.command()
    async def whosagoodgirl(self, ctx):
        'Respondo si soy una niña buena.'
        await ctx.send('¡Yo! c:')

    @commands.command()
    async def ada(self, ctx):
        'Paso mi avatar.'
        await ctx.send(self.bot.user.avatar_url)

    @commands.command()
    async def elchino(self, ctx):
        'Grito si El Chino está jugando al Warframe.'
        usuario_elchino = ctx.guild.get_member(224295650072854528)
        if usuario_elchino:
            if usuario_elchino.activity.name == 'Warframe':
                retorno = 'EL CHINO.'
            else:
                retorno = 'El Chino.'
        else:
            retorno = 'No juno Chino.'
        await ctx.send(retorno)

    @commands.command()
    async def rola(self, ctx, *, member: discord.Member=None):
        'Si vos o el miembro que me decís está usando Spotify, determino si se trata de una buena rola o no (del 0 al 10).'
        if member is None:
            member = ctx.message.author
        if isinstance(member.activity, discord.Spotify):
            #cadena_concatenada = ''.join(member.activity.artists + [member.activity.title])
            cadena_unicode = [ord(caracter) for caracter in member.activity.title]
            cadena_restos = [((numero_unicode % 10) + 1) for numero_unicode in cadena_unicode]
            puntaje = sum(cadena_restos) / len(cadena_restos)
            opinion = ''
            if puntaje < 6:
                opinion = ':grimacing:'
            if (puntaje >= 6) and (puntaje < 8):
                opinion = ':grin:'
            if puntaje >= 8:
                opinion = ':sunglasses:'
            retorno = f'''**{member.display_name}:**\nRolando **{member.activity.title}** de **{', '.join(member.activity.artists)}**. c:\nPuntaje de la canción: {puntaje}\n{opinion}'''
        else:
            retorno = 'No rolando. :c'
        await ctx.send(retorno)

    @commands.command()
    async def japish(self, ctx):
        'Paso basoooura.'
        lista_de_respuestas = ['JAPISH JAPISH.', 'ARRRGH.', 'BASOOOURA.']
        random.shuffle(lista_de_respuestas)
        await ctx.send(lista_de_respuestas[0])

    @commands.command()
    #async def latex(self, ctx, entrada: str='\LaTeX', bgcolor: str='transparent', textcolor: str='ffffff', textsize: str='4'):
    async def latex(self, ctx):
        '''Paso una imagen en base a una entrada en LaTeX.'''
        #OPCIONES EXTRA DEPRECADAS POR AHORA.
        #2do argumento: color de fondo en HEX (por defecto transparent)
        #3er argumento: color de texto en HEX (por defecto #FFFFFF)
        #4to argumento: tamaño de texto, de -4 a 4. (por defecto 4).'''
        #await ctx.send((((((('https://s0.wp.com/latex.php?latex=' + entrada.replace('+', '%2B')) + '&bg=') + bgcolor) + '&fg=') + textcolor) + '&s=') + textsize)
        url = 'https://s0.wp.com/latex.php?latex=' + ctx.message.content[7:].replace('+', '%2B') + '&bg=transparent&fg=ffffff&s=4'
        embed = discord.Embed()
        embed.set_image(url=url)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Miscelaneo(bot))
