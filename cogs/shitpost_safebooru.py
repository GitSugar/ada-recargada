
import discord
from discord.ext import commands
import asyncio
import aiohttp

class ShitpostSafebooru():

    def __init__(self, bot):
        self.bot = bot

    async def fetch_json_desde_url(self, session, url):
        async with session.get(url) as response:
            return await response.json()

    async def donmai_safebooru_shitpost_get(self, tags):
        'Consigo imágenes de Safebooru.donmai.us en base a tags de argumento.'
        async with aiohttp.ClientSession() as sesion:
            'Algunas rutas de imágenes no explicitan el subdominio de donmai.us en las que están almacenadas.'
            'Hasta que descubra alguna manera de conseguir dicho subdominio, esas imágenes serán descartadas.'
            'Por las dudas, ofrezco sólo 10 intentos.'
            url = '/'
            intento = 0
            while url[0] == '/' and intento < 9:
                json_parseado = await self.fetch_json_desde_url(sesion, 'https://safebooru.donmai.us/posts.json?limit=10000&random=true&tags=' + tags)
                url = json_parseado[0]['file_url']
                intento+=1
            if intento == 9:
                embed = discord.Embed(description="Explicitá tus subdominios, Safebooru. >:c")
                return embed
            embed = discord.Embed()
            embed.set_image(url=url)
            return embed

    @commands.command()
    async def ahegao(self, ctx):
        'Paso una imagen ahegao.'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('ahegao'))

    @commands.command()
    async def sakuya(self, ctx):
        'Paso una imagen de Sakuya Izayoi (para Siro).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('izayoi_sakuya+1girl'))

    @commands.command()
    async def futaba(self, ctx):
        'Paso una imagen de Futaba Sakura (para Iván).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('sakura_futaba+1girl'))

    @commands.command()
    async def giogio(self, ctx):
        'Paso una imagen de Giorno Giovanna (para Julián).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('giorno_giovanna'))

    @commands.command()
    async def nagisa(self, ctx):
        'Paso una imagen de Nagisa Hazuki (para Amz).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('hazuki_nagisa'))

    @commands.command()
    async def reisen(self, ctx):
        'Paso una imagen de Reisen Udongein Inaba (para Goofy).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('reisen_udongein_inaba+1girl'))

    @commands.command()
    async def sakurako(self, ctx):
        'Paso una imagen de Sakurako Oomuro (para Evo).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('oomuro_sakurako+1girl'))

    @commands.command()
    async def gappy(self, ctx):
        'Paso una imagen de Josuke "Gappy" Higashikata (para Jota).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('higashikata_jousuke_(jojolion)'))

    @commands.command()
    async def bulma(self, ctx):
        'Paso una imagen de Bulma (para Fetus).'
        await ctx.send(embed=await self.donmai_safebooru_shitpost_get('bulma+1girl'))

def setup(bot):
    bot.add_cog(ShitpostSafebooru(bot))
