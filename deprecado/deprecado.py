def safebooru_shitpost_get(tags):
	'''Consigo imágenes de Safebooru.org en base a tags de argumento.'''
	xml_original = requests.get("https://safebooru.org/index.php?page=dapi&s=post&q=index&tags="+tags)
	xml_parseado = et.fromstring(xml_original.text)
	lista_de_imagenes = []
	for imagen in xml_parseado:
		lista_de_imagenes.append(imagen)
	random.shuffle(lista_de_imagenes)
	return "https:" + lista_de_imagenes[0].attrib["file_url"]

def OLD_donmai_safebooru_shitpost_get(tags):
	'''Consigo imágenes de Safebooru.donmai.us en base a tags de argumento.'''
	json_original = requests.get("https://safebooru.donmai.us/posts.json?limit=10000&random=true&tags=" + tags)
	return "https://safebooru.donmai.us" + json_original.json()[0]["file_url"]
